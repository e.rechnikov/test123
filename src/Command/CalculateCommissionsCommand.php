<?php

declare(strict_types = 1);

namespace App\Command;

    use App\Service\Commissions;
    use Symfony\Component\Console\Command\Command;
    use Symfony\Component\Console\Input\InputArgument;
    use Symfony\Component\Console\Input\InputInterface;
    use Symfony\Component\Console\Output\OutputInterface;
    use Symfony\Component\Console\Style\SymfonyStyle;

    class CalculateCommissionsCommand extends Command
    {
        protected static $defaultName = 'calculate:commissions';
        /**
         * @var Commissions
         */
        private $commissions;

        public function configure()
        {
            $this->setDescription('Calculate Commissions');
            $this->addArgument(
                'filePath',
                InputArgument::REQUIRED,
                'Relative file input path'
            );
        }

        public function __construct(Commissions $commissions){
            $this->commissions = $commissions;
            parent::__construct();
        }

        public function execute(InputInterface $input, OutputInterface $output)
        {
            $io = new SymfonyStyle($input, $output);
            $io->section('Calculate');
            $filePath = $input->getArgument('filePath');
            $data = $this->commissions->calculate($filePath);
            $tableData = [];
            foreach ($data[$filePath] as $datum) {
                $tableData[] = [$datum];
            }
            $io->table(['Commissions'], $tableData);
            $io->success('Finished');

            return 1;
        }
    }
