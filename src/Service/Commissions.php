<?php

namespace App\Service;

use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class Commissions
{
    private $kernel;
    /**
     * @var Finder
     */
    private $finder;

    /**
     * @var ParameterBagInterface
     */
    private $config;
    /**
     * @var HttpClientInterface
     */
    private $httpClient;

    public function __construct(
        KernelInterface       $kernel,
        Finder                $finder,
        ParameterBagInterface $params,
        HttpClientInterface   $httpClient
    ) {
        $this->kernel = $kernel;
        $this->finder = $finder;
        $this->config = $params;
        $this->httpClient = $httpClient;
    }

    public function calculate($filePath)
    {
        $files = $this->finder->files()->in($this->kernel->getProjectDir())->name($filePath);

        $result = [];
        foreach ($files as $file) {
            $rows = explode("\n", $file->getContents());
            $result[$file->getFilename()] = $this->calculateRows($rows);
        }

        return $result;
    }

    /**
     * @param array $rows
     * @return array
     */
    private function calculateRows(array $rows) : array
    {
        $res = [];
        foreach ($rows as $row) {
            if (empty($row)) continue;
            $data = json_decode($row, true);
            $country = $this->getCountry((int) $data['bin']);
            $amount = $this->getCalculatedAmount($data['amount'], $data['currency']);
            $isEu = $this->isEu($country);
            $amount2 = $isEu ?
                $amount * $this->config->get('app.euRate') :
                $amount * $this->config->get('app.nonEuRate');
            $res[] = round($amount2, 2);
        }


        return $res;
    }

    /**
     * @param int $bin
     * @return mixed
     */
    private function getCountry(int $bin)
    {
        $data = $this->makeApiCall($this->config->get('app.binService') . $bin);
        return $data['country']['alpha2'];
    }

    /**
     * @param $country
     * @return bool
     */
    private function isEu($country)
    {
        $euCountries = $this->config->get('app.euCountries');
        return in_array($country, $euCountries);
    }

    /**
     * @param $amount
     * @param $currency
     * @return float|int
     */
    private function getCalculatedAmount($amount, $currency)
    {
        $data = $this->makeApiCall($this->config->get('app.exchangeService'));


        if (strtoupper($currency) == 'EUR' || $data['rates'][$currency] == 0) {
            return $amount;
        }

        return $amount / $data['rates'][$currency];
    }

    /**
     * @param string $url
     * @param string $method
     * @return mixed
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    private function makeApiCall(string $url, string $method = 'GET')
    {
        $result = $this->httpClient->request($method, $url);

        if ($result->getStatusCode() !== 200) {
            throw new \Exception('Error making request to ' . $url);
        }

        return json_decode($result->getContent(), true);
    }
}
