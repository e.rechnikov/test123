<?php

namespace App\Tests\Service;

use App\Kernel;
use App\Service\Commissions;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\DependencyInjection\ParameterBag;
use Symfony\Component\HttpClient\Response\MockResponse;

class CommissionsTest extends TestCase
{
    private $testData = '{"bin":"45717360","amount":"100.00","currency":"EUR"}';
    private $testDataNonEu = '{"bin":"516793","amount":"50.00","currency":"USD"}';

    private $testBinResponse = '{"number":{"length":16,"luhn":true},"scheme":"visa","type":"debit","brand":"Visa/Dankort","prepaid":false,"country":{"numeric":"208","alpha2":"DK","name":"Denmark","emoji":"","currency":"DKK","latitude":56,"longitude":10},"bank":{"name":"Jyske Bank","url":"www.jyskebank.dk","phone":"+4589893300","city":"Hjørring"}}';
    private $testExchangeRates = '{"rates":{"CAD":1.5488,"HKD":8.8171,"ISK":159.4,"PHP":56.305,"DKK":7.4447,"HUF":355.48,"CZK":26.638,"AUD":1.6348,"RON":4.8435,"SEK":10.3855,"IDR":16436.88,"INR":85.7725,"BRL":6.0925,"RUB":80.837,"HRK":7.5295,"JPY":122.17,"THB":35.922,"CHF":1.0691,"SGD":1.5846,"PLN":4.4781,"BGN":1.9558,"TRY":7.8111,"CNY":7.9805,"NOK":10.711,"NZD":1.7417,"ZAR":19.0732,"USD":1.1375,"MXN":25.7383,"ILS":3.9092,"GBP":0.90778,"KRW":1371.98,"MYR":4.8588},"base":"EUR","date":"2020-07-14"}';

    public function testCalculate()
    {
        $kernelMock = $this->getMockBuilder(Kernel::class)
            ->disableOriginalConstructor()
            ->getMock();

        $finderMock = $this->getMockBuilder(Finder::class)
            ->disableOriginalConstructor()
            ->getMock();

        $configMock = $this->getMockBuilder(ParameterBag\ParameterBag::class)
            ->disableOriginalConstructor()
            ->getMock();

        $configMock->method('get')->withAnyParameters()->willReturnMap([
            ['app.binService', 'testBinUrl'],
            ['app.exchangeService', 'testUrlExchange'],
            ['app.euCountries', ['AT', 'BE', 'BG']],
            ['app.euRate', 0.01],
            ['app.nonEuRate', 0.02],
        ]);


        $httpClientMock = $this->getMockBuilder(MockHttpClient::class)
            ->disableOriginalConstructor()
            ->getMock();

        $responseMock = $this->getMockBuilder(MockResponse::class)
            ->disableOriginalConstructor()
            ->getMock();

        $responseMock->method('getContent')->withAnyParameters()->willReturn($this->testBinResponse);
        $responseMock->method('getStatusCode')->willReturn(200);

        $responseMock2 = $this->getMockBuilder(MockResponse::class)
            ->disableOriginalConstructor()
            ->getMock();

        $responseMock2->method('getContent')->willReturn($this->testExchangeRates);
        $responseMock2->method('getStatusCode')->willReturn(200);


        $httpClientMock->expects($this->at(0))
            ->method('request')
            ->with('GET', 'testBinUrl45717360')
            ->willReturn($responseMock);

        $httpClientMock->expects($this->at(1))
            ->method('request')
            ->with('GET', 'testUrlExchange')
            ->willReturn($responseMock2);

        $fileMock = $this->getMockBuilder(SplFileInfo::class)
            ->disableOriginalConstructor()
            ->getMock();

        $fileMock->method('getContents')->willReturn($this->testData);
        $fileMock->method('getFilename')->willReturn('testFilePath');

        $finderMock->method('getIterator')->willReturn(new \ArrayIterator([$fileMock]));
        $finderMock->method('in')->withAnyParameters()->willReturn($finderMock);
        $finderMock->method('files')->willReturn($finderMock);
        $finderMock->method('name')->willReturn($finderMock);

        $sut = new Commissions($kernelMock, $finderMock, $configMock, $httpClientMock);
        $res = $sut->calculate('testFilePath');

        $this->assertEquals(2, $res['testFilePath'][0]);
    }

    public function testCalculateNonEU()
    {
        $kernelMock = $this->getMockBuilder(Kernel::class)
            ->disableOriginalConstructor()
            ->getMock();

        $finderMock = $this->getMockBuilder(Finder::class)
            ->disableOriginalConstructor()
            ->getMock();

        $configMock = $this->getMockBuilder(ParameterBag\ParameterBag::class)
            ->disableOriginalConstructor()
            ->getMock();

        $configMock->method('get')->withAnyParameters()->willReturnMap([
           ['app.binService', 'testBinUrl'],
           ['app.exchangeService', 'testUrlExchange'],
           ['app.euCountries', ['AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GR', 'HR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PO', 'PT', 'RO', 'SE', 'SI', 'SK']],
           ['app.euRate', 0.01],
           ['app.nonEuRate', 0.02],
       ]);


        $httpClientMock = $this->getMockBuilder(MockHttpClient::class)
            ->disableOriginalConstructor()
            ->getMock();

        $responseMock = $this->getMockBuilder(MockResponse::class)
            ->disableOriginalConstructor()
            ->getMock();

        $responseMock->method('getContent')->withAnyParameters()->willReturn($this->testBinResponse);
        $responseMock->method('getStatusCode')->willReturn(200);

        $responseMock2 = $this->getMockBuilder(MockResponse::class)
            ->disableOriginalConstructor()
            ->getMock();

        $responseMock2->method('getContent')->willReturn($this->testExchangeRates);
        $responseMock2->method('getStatusCode')->willReturn(200);


        $httpClientMock->expects($this->at(0))
            ->method('request')
            ->with('GET', 'testBinUrl516793')
            ->willReturn($responseMock);

        $httpClientMock->expects($this->at(1))
            ->method('request')
            ->with('GET', 'testUrlExchange')
            ->willReturn($responseMock2);

        $fileMock = $this->getMockBuilder(SplFileInfo::class)
            ->disableOriginalConstructor()
            ->getMock();

        $fileMock->method('getContents')->willReturn($this->testDataNonEu);
        $fileMock->method('getFilename')->willReturn('testFilePath');

        $finderMock->method('getIterator')->willReturn(new \ArrayIterator([$fileMock]));
        $finderMock->method('in')->withAnyParameters()->willReturn($finderMock);
        $finderMock->method('files')->willReturn($finderMock);
        $finderMock->method('name')->willReturn($finderMock);

        $sut = new Commissions($kernelMock, $finderMock, $configMock, $httpClientMock);
        $res = $sut->calculate('testFilePath');

        $this->assertEquals(0.44, $res['testFilePath'][0]);
    }

    public function testApiThrowException()
    {
        $kernelMock = $this->getMockBuilder(Kernel::class)
            ->disableOriginalConstructor()
            ->getMock();

        $finderMock = $this->getMockBuilder(Finder::class)
            ->disableOriginalConstructor()
            ->getMock();

        $configMock = $this->getMockBuilder(ParameterBag\ParameterBag::class)
            ->disableOriginalConstructor()
            ->getMock();

        $configMock->method('get')->withAnyParameters()->willReturnMap([
           ['app.binService', 'testBinUrl'],
           ['app.exchangeService', 'testUrlExchange'],
           ['app.euCountries', ['AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GR', 'HR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PO', 'PT', 'RO', 'SE', 'SI', 'SK']],
           ['app.euRate', 0.01],
           ['app.nonEuRate', 0.02],
       ]);


        $httpClientMock = $this->getMockBuilder(MockHttpClient::class)
            ->disableOriginalConstructor()
            ->getMock();

        $responseMock = $this->getMockBuilder(MockResponse::class)
            ->disableOriginalConstructor()
            ->getMock();

        $responseMock->method('getContent')->withAnyParameters()->willReturn($this->testBinResponse);
        $responseMock->method('getStatusCode')->willReturn(500);

        $httpClientMock->expects($this->at(0))
            ->method('request')
            ->with('GET', 'testBinUrl516793')
            ->willReturn($responseMock);

        $fileMock = $this->getMockBuilder(SplFileInfo::class)
            ->disableOriginalConstructor()
            ->getMock();

        $fileMock->method('getContents')->willReturn($this->testDataNonEu);
        $fileMock->method('getFilename')->willReturn('testFilePath');

        $finderMock->method('getIterator')->willReturn(new \ArrayIterator([$fileMock]));
        $finderMock->method('in')->withAnyParameters()->willReturn($finderMock);
        $finderMock->method('files')->willReturn($finderMock);
        $finderMock->method('name')->willReturn($finderMock);
        $this->expectExceptionMessage('Error making request to testBinUrl516793');

        $sut = new Commissions($kernelMock, $finderMock, $configMock, $httpClientMock);
        $sut->calculate('testFilePath');
    }
}
